import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 256.0),
                child: CircleAvatar(
                  radius: 50.0,
                  backgroundColor: Colors.purple,
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                    size: 64.0,
                    semanticLabel: 'Text to announce in accessibility modes',
                  ),
                )),
            Padding(
                child: Text(
                  "Awesome",
                  style: TextStyle(fontSize: 26.0, fontWeight: FontWeight.bold),
                ),
                padding: EdgeInsets.only(top: 16.0)),
            Padding(
              child: Text(
                "Your profile looks great, ready to\nlook around.",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14.0),
              ),
              padding: EdgeInsets.only(top: 24.0),
            ),
            Padding(
                padding: EdgeInsets.only(top: 128.0),
                child: MaterialButton(
                    child: new Text("DISCOVER", style: TextStyle(fontWeight:FontWeight.bold ),),
                    minWidth: 250.0,
                    color: Colors.yellow,
                    onPressed:() {
                      print("Button pressed");
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0))))
          ],
        )
      ],
    ));
  }
}
